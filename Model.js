var Model = function () {
    this.name = {
        value: null,
        validator: ['isString', 'isNotEmpty']
    };
    this.email = {
        value: null,
        validator: ['isString', 'isNotEmpty']
    };
    this.password = {
        value: null,
        validator: ['isNotEmpty', 'isString']
    };
};

Model.prototype.check = function (value, key) {
    var validating = new Validator();
    if (validating.validate(value, key.validator)) {

        key.value = value;
        return true;
    }
    return false;
};

Model.prototype.checkUserName = function (name) {
    return this.check(name, this.name);
};

Model.prototype.checkEmail = function (email) {
    return this.check(email, this.email);
};
Model.prototype.checkPassword = function (password) {
    return this.check(password, this.password);
};
var selectedRow = null;
const scriptURL =
    'https://script.google.com/macros/s/AKfycbw_lmHRmOeIcHYcUypozYIfELeE-WZYn39KGxFJXAJDX8SA2lpus-0BMI9boSVh4crN/exec';

var form = document.getElementById('form');

function onFormSubmit() {
    var formData = readFormData();
    if (selectedRow == null)
        insertNewRecordInSheet(formData);
    else {
        updateRecord(formData)
        updateRecordInSheet(formData);
    }
    resetForm();
}



function updateRecordInSheet() {
    var action = document.getElementById('action');
    action.value = 'MemberUpdateRequest';
    var pid = document.getElementById('projectid');
    pid.value = sessionStorage.getItem('selectedproject');
    console.log(document.getElementById('email').value);
    console.log(document.getElementById('role').value)
    console.log(pid.value)
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(form)
        })
        .then(res => res.json()).then(rep => {
            alert(rep.send)
            action.value = 'MemberAddRequest';

        })
        .catch(error => {
            action.value = 'MemberAddRequest';
            console.error('Error!', error.message)
        })
    action.value = 'MemberAddRequest';
    document.getElementById('email').disabled = false;

}

function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.email;
    selectedRow.cells[1].innerHTML = formData.role;

}

function insertNewRecordInSheet(Data) {
    var pid = document.getElementById('projectid');
    pid.value = sessionStorage.getItem('selectedproject');
    console.log(document.getElementById('role').value)
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(form)
        })
        .then(res => res.json()).then(rep => {

            if (!(rep.data)) {
                alert('User Not Present');
                return
            }
            insertNewRecord(Data);
        })
        .catch(error => console.error('Error!', error.message))
}

function insertNewRecord(data) {
    var table = document.getElementById("table").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.email;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.role;
    cell3 = newRow.insertCell(2);
    var a1 = document.createElement('a');
    var a2 = document.createElement('a');
    a1.setAttribute('onClick', 'onEdit(this)');
    a2.setAttribute('onClick', 'onDelete(this)');
    a1.innerText = 'Edit';
    a2.innerText = 'Delete';
    cell3.appendChild(a1)
    cell3.appendChild(a2)
    console.log(cell3.innerHTML)
    var role = sessionStorage.getItem('role');


    if (!hasPermission(role, actions.DELETE_STORY))
        a2.style.display = "none";
    else
        a2.style.display = 'visible'

    if (!hasPermission(role, actions.MODIFY_STORY))
        a1.style.display = "none";
    else
        a1.style.display = 'visible'
    console.log(role);
    console.log(a2.style.display)


}

function resetForm() {
    document.getElementById("email").value = "";
    document.getElementById("role").value = "Admin";
    selectedRow = null;

}



function readFormData() {
    var formData = {};
    formData["email"] = document.getElementById("email").value;
    formData["role"] = document.getElementById("role").value;
    formData["pid"] = sessionStorage.getItem('selectedproject');
    return formData;

}


function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("email").value = selectedRow.cells[0].innerHTML;
    document.getElementById("role").value = selectedRow.cells[1].innerHTML;
    document.getElementById('projectid').value = sessionStorage.getItem('selectedproject')
    document.getElementById('email').readOnly = true;


}

function onDelete(td) {
    var myform = document.getElementById('form_delete');
    var user = document.getElementById('user');
    user.value = td.parentElement.parentElement.firstChild.innerHTML;
    console.log(user.value)
    selectedRow = td.parentElement.parentElement;
    document.getElementById("deletememberprojectid").value = sessionStorage.getItem('selectedproject')
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(myform)
        })
        .then(res => res.json()).then(rep => {
            alert(rep.data)
            console.log(rep.deleted)
            if (!(rep.deleted)) {
                return false;
            }
        })
        .catch(error => {
            console.error('Error!', error.message)
        })

    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("table").deleteRow(row.rowIndex);
        resetForm();
    }
}
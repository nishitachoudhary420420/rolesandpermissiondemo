var count = 1;
var myform = document.getElementById('form_project');
var email = document.getElementById('email');
email.value = sessionStorage.getItem('user');;
var user = document.getElementById('user');
user.value = email.value;
const scriptURL =
    'https://script.google.com/macros/s/AKfycbw_lmHRmOeIcHYcUypozYIfELeE-WZYn39KGxFJXAJDX8SA2lpus-0BMI9boSVh4crN/exec';

function onPageLoad() {
    fetch(scriptURL, {
        method: 'POST',
        body: new FormData(myform),
    }).then(res => res.json()).then(rep => {
        var arr = rep.data;
        if (arr == null || arr == '') {
            alert('No Projects Created Yet')
            return;
        }
        for (var i = 0; i < arr.length; i++) {
            var element = createElement(arr[i].title, arr[i].description, arr[i].id, count, (arr[i].role)
                .toUpperCase());
            document.getElementById("contain").appendChild(element);
            count++;
        }


    })
};

function createElement(title, description, id, count, role) {
    var div1 = document.createElement('div');
    var div2 = document.createElement('div');
    var div3 = document.createElement('div');
    var div4 = document.createElement('div');
    var div5 = document.createElement('div');

    var h5 = document.createElement('h5');
    var p = document.createElement('p');
    var button1 = document.createElement('button');
    var button2 = document.createElement('button');
    var button3 = document.createElement('button');
    div4.innerText = id;
    h5.innerText = title;
    p.innerText = description;
    div2.innerText = `Project${count}`
    div1.classList.add("card", "text-white", "bg-dark", "mb-3");
    div2.classList.add("card-header");
    div3.classList.add("card-body");
    div4.style.display = 'none';
    div4.classList.add('ids');
    div5.innerHTML = role.toUpperCase();
    div5.style.display = 'inline-block';
    div5.style.float = 'right'
    h5.classList.add("card-title", "card-color");
    p.classList.add("card-text");
    button1.classList.add("btn", "btn-primary", "buttons", "mr-2");
    button1.innerText = 'View';
    button1.setAttribute('onclick', 'View(this)');
    button2.classList.add("btn", "btn-primary", "buttons");
    button2.innerText = 'Delete';
    button2.setAttribute('onclick', 'Delete(this)');
    button3.innerText = 'Members';
    button3.setAttribute('onclick', 'Add_Member(this)');
    button3.classList.add("btn", "btn-primary", "buttons", "ml-2")


    if (!hasPermission(role, actions.DELETE_STORY))
        button2.style.display = "none";
    else
        button2.style.display = 'visible'

    if (role == 'EDITOR') {
        button2.style.display = 'none'
    }

    div3.appendChild(h5);
    div3.appendChild(p);
    div3.appendChild(button1);
    div3.appendChild(div4);
    div3.appendChild(button2);
    div3.appendChild(button3);
    div3.appendChild(div5);

    div1.appendChild(div2);
    div1.appendChild(div3);

    return div1;
}

function View(button) {
    console.log(button.nextSibling.textContent)
    sessionStorage.setItem('selectedproject', button.nextSibling.textContent);
    sessionStorage.setItem('role', button.nextSibling.nextSibling.nextSibling.nextSibling.innerText)
    window.location = 'Task.html';
}

function Delete(button) {
    console.log(button.previousSibling.textContent)
    sessionStorage.setItem('selectedproject', button.previousSibling.textContent);
    onDeleteProject(button);

}

function logout() {
    sessionStorage.clear();
    window.location = 'index.html'
}

function Add_Member(button) {
    console.log(button.previousSibling.previousSibling.textContent)
    sessionStorage.setItem('selectedproject', button.previousSibling.previousSibling.textContent);
    sessionStorage.setItem('role', button.nextSibling.innerText)
    window.location = 'AddMembers.html'
}

function onDeleteProject(button) {
    var myform = document.getElementById('form_delete');
    var deletepid = sessionStorage.getItem('selectedproject');
    document.getElementById('projectid').value = deletepid;
    document.getElementById('user').value = sessionStorage.getItem('user');
    if (!confirm('Are you sure to delete this record ?')) {
        return;
    }
    row = button.parentElement.parentElement;
    row.remove();
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(myform)
        })
        .then(res => res.json()).then(rep => {
            alert(rep.status);
            console.log(rep.send)
        })
        .catch(error => {
            console.error('Error!', error.message)
        })
}
var selectedRow = null

const scriptURL =
    'https://script.google.com/macros/s/AKfycbw_lmHRmOeIcHYcUypozYIfELeE-WZYn39KGxFJXAJDX8SA2lpus-0BMI9boSVh4crN/exec';
const form = document.forms['form'];
const form_delete = document.forms['form_delete'];
const form_task = document.forms['form_task'];
const title = document.getElementById("title")
const description = document.getElementById("description")
const assignees = document.getElementById("assignees");
const statuss = document.getElementById("status");
const tcpid = document.getElementById('task_creation_project_id');


function onFormSubmit() {
    var formData = readFormData();
    if (selectedRow == null)
        insertNewRecordInSheet(formData);
    else {
        updateRecord(formData)
        updateRecordInSheet(formData);
    }
    resetForm();
}

function insertNewRecordInSheet(Data) {
    tcpid.value = sessionStorage.getItem('selectedproject');
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(form)
        })
        .then(res => res.json()).then(rep => {
            insertNewRecord(Data, rep.id);
        })
        .catch(error => console.error('Error!', error.message))
}

function updateRecordInSheet(formData) {
    action.value = 'TaskUpdateRequest'
    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(form)
        })
        .then(res => res.json()).then(rep => {
            alert(rep.status)
            action.value = 'TaskAddRequest';

        })
        .catch(error => {
            action.value = 'TaskAddRequest';
            console.error('Error!', error.message)
        })
    action.value = 'TaskAddRequest';

}

function readFormData() {
    var formData = {};
    formData["title"] = document.getElementById("title").value;
    formData["description"] = document.getElementById("description").value;
    formData["assignees"] = document.getElementById("assignees").value;
    formData["status"] = document.getElementById("status").value;
    formData["id"] = document.getElementById("task_id").value;

    return formData;

}

function insertNewRecord(data, id) {
    var table = document.getElementById("table").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.title;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.description;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.assignees;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.status;
    cell4 = newRow.insertCell(4);
    cell4.innerHTML = `<a onClick="onEdit(this)">Edit</a>
                       <a onClick="onDelete(this)">Delete</a>`;
    cell5 = newRow.insertCell(5);
    cell5.innerHTML = id;
    cell5.style.display = 'none';

}

function resetForm() {
    document.getElementById("title").value = "";
    document.getElementById("description").value = "";
    document.getElementById("assignees").value = "";
    document.getElementById("status").value = "";
    document.getElementById("task_id").value = "";
    selectedRow = null;
}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("title").value = selectedRow.cells[0].innerHTML;
    document.getElementById("description").value = selectedRow.cells[1].innerHTML;
    document.getElementById("assignees").value = selectedRow.cells[2].innerHTML;
    document.getElementById("status").value = selectedRow.cells[3].innerHTML;
    document.getElementById("task_id").value = selectedRow.cells[5].innerHTML

}

function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.title;
    selectedRow.cells[1].innerHTML = formData.description;
    selectedRow.cells[2].innerHTML = formData.assignees;
    selectedRow.cells[3].innerHTML = formData.status;
    selectedRow.cells[5].innerHTML = formData.id;
}

function onDelete(td) {
    var myform = document.getElementById('form_delete');
    selectedRow = td.parentElement.parentElement;
    document.getElementById("deletetaskid").value = selectedRow.cells[5].innerHTML;

    fetch(scriptURL, {
            method: 'POST',
            body: new FormData(myform)
        })
        .then(res => res.json()).then(rep => {
            alert(rep.status)
        })
        .catch(error => {
            console.error('Error!', error.message)
        })

    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("table").deleteRow(row.rowIndex);
        resetForm();
    }
}
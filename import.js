import hasPermission from "./permissions.js";
import {
    actions
} from "./constants.js";
window.hasPermission = hasPermission;
window.actions = actions;